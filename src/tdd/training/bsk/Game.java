package tdd.training.bsk;

public class Game {
	
	private static final int MAX_FRAMES = 10;
	
	private Frame[] frames;
	private int lastFrameIndex = 0;
	private int firstBonusThrow = 0;
	private int secondBonusThrow = 0;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		this.frames = new Frame[MAX_FRAMES];
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		
		if(lastFrameIndex >= MAX_FRAMES) {
			throw new BowlingException();
		}
		
		frames[lastFrameIndex] = frame;
		lastFrameIndex++;
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if(index > lastFrameIndex) {
			throw new BowlingException();
		}
		
		return this.frames[index];
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int totalScore = 0;
		
		for(int i=0; i<frames.length; i++) {
			totalScore += frames[i].getScore();
			
			if(i!=frames.length-1) {
				int bonus = 0;
				
				if(frames[i].isSpare()) {
					bonus = frames[i+1].getFirstThrow();
				}
				else if(frames[i].isStrike()) {
					bonus = frames[i+1].getFirstThrow() + frames[i+1].getSecondThrow();
				
					if(i!=frames.length-2) {
						if(frames[i+1].isStrike()) {
							bonus += frames[i+2].getFirstThrow();
						}
					}
					else {
						bonus += firstBonusThrow;
					}
				}
				frames[i].setBonus(bonus);
				totalScore += frames[i].getBonus();
			}
		}
		
		return totalScore + firstBonusThrow + secondBonusThrow;
	}

}
