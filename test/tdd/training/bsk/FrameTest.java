package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	public void frameShouldReturnFirstThrow() throws Exception{
		Frame frame = new Frame(3, 1);
		
		assertEquals(3, frame.getFirstThrow());
	}
	
	
	@Test
	public void frameShouldReturnSecondThrow() throws Exception{
		Frame frame = new Frame(1, 3);
		
		assertEquals(3, frame.getSecondThrow());
	}
	
	
	@Test(expected=BowlingException.class)
	public void negativeFirstThrowShouldRaiseException() throws Exception{
		new Frame(-1, 2);
	}
	
	
	@Test(expected=BowlingException.class)
	public void negativeSecondThrowShouldRaiseException() throws Exception{
		new Frame(2, -1);
	}
	
	
	@Test(expected=BowlingException.class)
	public void moreOf10PinsPerFrameShouldRaiseException() throws Exception{
		new Frame(8, 3);
	}
	
	
	@Test
	public void testScore() throws Exception{
		Frame frame = new Frame(1, 3);
		
		assertEquals(4, frame.getScore());
	}
	
	
	@Test
	public void testSpare() throws Exception{
		Frame frame = new Frame(2, 8);
		
		assertTrue(frame.isSpare());
	}
	
	
	@Test
	public void testBonusAfterSpare() throws Exception{
		Frame spare = new Frame(2, 8);
		Frame subsequent = new Frame(3, 6);
		
		spare.setBonus(subsequent.getFirstThrow());
		
		assertEquals(3, spare.getBonus());
	}
	
	
	@Test
	public void testScoreAfterSpareBonus() throws Exception{
		Frame spare = new Frame(2, 8);
		Frame subsequent = new Frame(3, 6);
		
		spare.setBonus(subsequent.getFirstThrow());
		
		assertEquals(13, spare.getScore());
	}
	
	
	@Test
	public void testStrike() throws Exception{
		Frame frame = new Frame(10, 0);
		
		assertTrue(frame.isStrike());
	}
	
	
	@Test
	public void testScoreAfterStrikeBonus() throws Exception{
		Frame strike = new Frame(10, 0);
		Frame subsequent = new Frame(3, 6);
		
		strike.setBonus(subsequent.getFirstThrow() + subsequent.getSecondThrow());
		
		assertEquals(19, strike.getScore());
	}

}
